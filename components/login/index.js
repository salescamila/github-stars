import { StyledContainer } from './style'
import { useEffect, useState } from 'react'

export default function Login(props) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [loginEnabled, setLoginEnabled] = useState(false)

    useEffect(() => {
        setLoginEnabled(username.length > 0 && password.length > 0)
    }, [username, password])

    const login = () => {
        // To-do: authenticate with github
        props.setLogged(true)
    }

    return (
        <StyledContainer>
            <h2>Login at GitHub</h2>
            <input
                id="username"
                placeholder="Username"
                onChange={(e) => setUsername(e.target.value.trim())}
                onPaste={(e) => setUsername(e.target.value.trim())}
                type="text"
                value={username}
            />
            <input
                id="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value.trim())}
                onPaste={(e) => setPassword(e.target.value.trim())}
                onKeyPress={(e) => e.key == 'Enter' && login()}
                type="password"
                value={password}
            />
            <br />
            <button onClick={login} disabled={!loginEnabled}>
                Login
            </button>
        </StyledContainer>
    )
}
