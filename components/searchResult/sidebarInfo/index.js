import Image from 'next/image'
import { StyledContainer } from './style'

export default function SidebarInfo(props) {
    const { name, username, bio, followers, city, email, site } = props.user

    return (
        <StyledContainer>
            <div className="username">
                <Image
                    id="title-project"
                    src="/image/profile.png"
                    alt="Title icon"
                    height={160}
                    width={160}
                />

                <p id="name">{name}</p>
                <p id="username">{username}</p>
            </div>

            <div className="information">
                {bio}
                <div className="contactInfo">
                    {followers ? (
                        <p>
                            {' '}
                            <Image
                                id="users"
                                src="/icons/users.svg"
                                alt="Users icon"
                                height={12}
                                width={14}
                            />{' '}
                            {followers[0]} {followers[1]} {followers[2]}{' '}
                            {followers.length > 3 ? '...' : ''}
                        </p>
                    ) : (
                        ''
                    )}

                    {city ? (
                        <p>
                            {' '}
                            <Image
                                id="map-pin"
                                src="/icons/map-pin.svg"
                                alt="Map pin icon"
                                height={15}
                                width={12}
                            />{' '}
                            {city}
                        </p>
                    ) : (
                        ''
                    )}

                    {email ? (
                        <p>
                            {' '}
                            <Image
                                id="mail"
                                src="/icons/mail.svg"
                                alt="Mail icon"
                                height={11}
                                width={13}
                            />{' '}
                            {email}
                        </p>
                    ) : (
                        ''
                    )}

                    {site ? (
                        <p>
                            {' '}
                            <Image
                                id="globe"
                                src="/icons/globe.svg"
                                alt="Globe icon"
                                height={13}
                                width={13}
                            />{' '}
                            {site}
                        </p>
                    ) : (
                        ''
                    )}
                </div>
            </div>
        </StyledContainer>
    )
}
