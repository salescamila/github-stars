import styled from 'styled-components'

export const StyledContainer = styled.div`
    border-radius: 5px;
    color: #fff;

    display: flex;
    flex-direction: column;
    text-align: center;
    margin-top: 32px;
    margin-bottom: 50px;
    width: 260px;

    .username {
        background: #5149a9;
        border-radius: 5px 5px 0px 0px;
        padding: 27px;

        #name {
            font-family: Roboto;
            font-style: normal;
            font-weight: normal;
            font-size: 20px;
            line-height: 23px;
            margin-bottom: 0px;
        }

        #username {
            font-family: Roboto;
            font-style: normal;
            font-weight: 300;
            font-size: 20px;
            line-height: 23px;
            margin: 0px;
        }
    }

    .information {
        background: #5253b9;
        border-radius: 0px 0px 5px 5px;
        padding: 30px 16px 0px;
        text-align: left;
        word-wrap: break-word;

        .contactInfo {
            padding: 10px;
        }
    }
`
