import Image from 'next/image'
import { StyledContainer } from './style'

export default function ProjectCard(props) {
    const { repository } = props

    function favoriteProject() {
        // To-do: favorite project 
    }

    return (
        <StyledContainer>
            <div className="information">
                <p id="projectTitle">{repository.username} / {repository.repository}</p>
                <p id="projectDescription">
                    {repository.description}
                </p>
                <span>
                    <Image
                        id="star"
                        src="/icons/star.svg"
                        alt="Star icon"
                        height={16}
                        width={16}
                    />{' '}
                    {repository.stars}
                </span>
            </div>
            <button onClick={favoriteProject()}>star</button>
        </StyledContainer>
    )
}
