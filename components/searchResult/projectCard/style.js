import styled from 'styled-components'

export const StyledContainer = styled.div`
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.15);
    border-radius: 5px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 27px 32px 32px;
    margin-bottom: 32px;

    p {
        margin: 12px 0px;
    }

    #projectTitle {
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 20px;
        line-height: 23px;
        margin-top: 0px;
    }

    #projectDescription {
        font-family: Roboto;
        font-style: normal;
        font-weight: 300;
        font-size: 16px;
        line-height: 19px;
    }
`
