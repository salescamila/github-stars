import { StyledContainer, StyledProjects } from './style'
import SidebarInfo from './sidebarInfo'
import ProjectCard from './projectCard'

export default function SearchResult(props) {
    const { user } = props
    return (
        <StyledContainer>
            <SidebarInfo user={user} />

            <StyledProjects>
                {user.starredRepositories?.map((repo, index) => {
                    return <ProjectCard key={index} repository={repo} />
                })}
            </StyledProjects>
        </StyledContainer>
    )
}
