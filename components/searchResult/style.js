import styled from 'styled-components'

export const StyledContainer = styled.div`
    display: flex;
`

export const StyledProjects = styled.div`
    box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
    border-radius: 5px;
    color: #4f4f4f;
    padding: 32px 32px 0px;
    width: 55vw;
`
