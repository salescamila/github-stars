import { StyledContainer } from './style'
import { useEffect, useState } from 'react'
import Search from '../search'
import SearchResult from '../searchResult'

export default function MainPage(props) {
    const [userFounded, setUserFounded] = useState('')

    return (
        <>
            {userFounded ? (
                <SearchResult user={userFounded} />
            ) : (
                <Search setUserFounded={setUserFounded} />
            )}
        </>
    )
}
