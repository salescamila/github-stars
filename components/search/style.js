import styled from 'styled-components'

export const StyledContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    
    input {
        margin-top: 54px;
        height: 91px;
        width: 60vw;

        background: #fff 0% 0% no-repeat padding-box;
        background-image: url('/icons/search.svg');
        background-position: right;
        background-position-x: 96%;
    }
`
