import Image from 'next/image'
import { StyledContainer } from './style'
import { useEffect, useState } from 'react'
import { userMock } from '../../pages/api/data'

export default function Search(props) {
    const [username, setUsername] = useState('')

    const searchUser = () => {
        // To-do: search for user at github
        if (username) {
            props.setUserFounded(userMock)
        }        
    }

    return (
        <StyledContainer>
            <Image
                id="title-project"
                src="/icons/GithubStars.svg"
                alt="Title icon"
                height={43}
                width={187}
            />
            <input
                id="username"
                placeholder="github username here..."
                onChange={(e) => setUsername(e.target.value.trim())}
                onPaste={(e) => setUsername(e.target.value.trim())}
                onKeyPress={(e) => e.key == 'Enter' && searchUser()}
                type="text"
                value={username}
            />
        </StyledContainer>
    )
}
