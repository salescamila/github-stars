export const userMock = {
    name: 'Sindre Sorhus',
    username: 'sindresorhus',
    bio: 'Maker · Full-Time Open-Sourcerer · Into Swift and Node.js',
    followers: ['@avajs', '@chalk', '@yeoman'],
    city: 'São Paulo, SP',
    email: 'sindresorhus@gmail.com',
    site: 'sindresorhus.com',
    starredRepositories: [
        {
            username: 'sindresorhus',
            repository: 'magic-interable',
            description:
                'Call a method on all items in an iterable by calling it on the iterable itself',
            stars: 200,
        },
        {
            username: 'chromium',
            repository: 'chromium',
            description: 'The official GitHub mirror of the Chromium source',
            stars: 58,
        },
        {
            username: 'atom',
            repository: 'xray',
            description: 'mafintosh / turbo-http',
            stars: 148,
        },
        {
            username: 'gsathya',
            repository: 'proposal-slice-notation',
            stars: 230,
        },
    ],
}
