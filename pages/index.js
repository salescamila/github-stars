import Head from 'next/head'
import dynamic from 'next/dynamic'
import styles from '../styles/Home.module.css'
import { useEffect, useState } from 'react'

const MainPage = dynamic(() => import('./../components/mainPage'), {
    ssr: false,
})

const Login = dynamic(() => import('./../components/login'), {
    ssr: false,
})

export default function Home() {
    const [logged, setLogged] = useState(false)

    useEffect(() => {
        checkSession()
    })

    function checkSession() {
        // To-do: verify if is logged at github
    }

    return (
        <div className={styles.container}>
            <Head>
                <title>GitHub Stars</title>
                <meta
                    content="width=device-width, initial-scale=1"
                    name="viewport"
                />
            </Head>

            {logged ? <MainPage /> : <Login setLogged={setLogged} />}
        </div>
    )
}
